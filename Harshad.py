
"""
Rakamları toplamına tam olarak bölünen sayılara HARSHAD (Niven) sayıları denir.

4240 ==> 4+2+4+0 = 10

4240/10 = 424 

"""

from colorama import Fore

class Harshad:
    
    def __init__(self):
        
        while True:
            self.Panel()
            self.Operation()
    
    def Panel(self):
        
        while True:
            self._input = input(Fore.WHITE+"Enter a Number:")
            
            if self._input.isdigit() == False:
                print(Fore.LIGHTRED_EX+ "Please Enter a Number ({})\n" .format(self._input))
                continue
            
            elif self._input.startswith("0") == True:
                print(Fore.LIGHTRED_EX+ "Enter a Different Number From Zero ({})\n" .format(self._input))
                continue
            
            else:
                break
                
    def Operation(self):
        self._list = []
        
        for i in self._input:
            self._list.append(int(i))
            
        self._result= sum(self._list)
        
        if int(self._input) % self._result == 0:
            print(Fore.GREEN +"{} The Number is a HARSHAD Number :) \n" .format(self._input))
           
        elif int(self._input) % self._result != 0 :
            print(Fore.LIGHTRED_EX + "{} The Number is not a HARSHAD Number :( \n" .format(self._input))
        
        else:
            print(Fore.RED+ "ERROR")
             
             
Harshad()